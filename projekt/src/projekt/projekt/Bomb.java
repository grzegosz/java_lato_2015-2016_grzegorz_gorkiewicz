package projekt;

import java.util.ArrayList;
import javax.imageio.ImageIO;

import java.awt.Graphics2D;
//import java.awt.*;
import java.awt.image.BufferedImage;

public class Bomb extends MapObject{
	
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {2,6};
	private static final int TICK  = 0;
	private static final int EXPLODE = 1;
	public boolean bum = false;
	private int range = 1;
	//private int time = 3000;
	//public Sound bombExp = new Sound("D:/Programy/Eclipse wokrspace/projekt/src/projekt/projekt/explosion.wav");
	public Sound bombExp = new Sound("src/projekt/projekt/explosion.wav");
	public Bomb(TileMap tm) {
		super(tm);
		width = 24;
		height = 24;
		cwidth = 32;
		cheight = 32;
		try {
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("bomb6.gif"));
			sprites = new ArrayList<BufferedImage[]>();
			for(int i = 0; i < 2; i++) {
				BufferedImage[] bi = new BufferedImage[numFrames[i]];
				if(i!=0)
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width +  (i)*numFrames[i-1] * width,0,width,height);
					}
				}
				else
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width,0,width,height);
					}
				}
				sprites.add(bi);	
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		animation = new Animation();
		currentAction = TICK;
		animation.setFrames(sprites.get(TICK));
		animation.setDelay(100);
		//bum = true;
	}
	
	public void expl(String dir, TileMap tm, Graphics2D g)
	{
	int col = this.getx()/cwidth;
	int row = this.gety()/cheight;
	int i=0, j=0;
	while(i<=range && i>=-range && j<=range && j>=-range)
	{
		if(col+i<15 && col+i>0 && row+j<15 && row+j>0)
		{
		if(tm.map[row+j][col+i]==1)
		{
			tm.map[row+j][col+i]=0;
			g.drawImage(animation.getImage(),(int)((col+i)*cwidth),(int)((row+j)*cheight),null);
			
		}
		else if(tm.map[row+j][col+i]==2)
		{
			g.drawImage(animation.getImage(),(int)((col+i)*cwidth),(int)((row+j)*cheight),null);
			tm.map[row+j][col+i]=0;
			return;
		}
		else if(tm.map[row+j][col+i]==3) return;
	}
		if(dir.equals("right")) 	i++;
		else if(dir.equals("left")) i--;
		else if(dir.equals("up")) 	j--;
		else if(dir.equals("down")) j++;
		else return;
	}
}
	
	public void explode(TileMap tm, Graphics2D g){
		bombExp.playSound();
		expl("right", tm, g);
		expl("left", tm, g);
		expl("up", tm, g);
		expl("down", tm, g);
		
		/*
		*/
		//}
		//}
	}
	
	public void Clear(TileMap tm)
	{
		int col = this.getx()/cwidth;
		int row = this.gety()/cheight;
		for(int i=-range; i<=range; i++)
		{
			for(int j=-range; j<=range; j++)
			{
				if(i==0 || j==0)
				{
					if(col+i<15 && col+i>0 && row+j<15 && row+j>0)
					{
						if(tm.map[row+j][col+i]==0)
							tm.map[row+j][col+i]=1;
					}
				}
			}
		}
	}
	
	public boolean hasCollision(TileMap tm, MapObject m){
		if(currentAction == EXPLODE){
			int col = this.getx()/cwidth;
			int row = this.gety()/cheight;
		for(int i=-range; i<=range; i++)
		{
			for(int j=-range; j<=range; j++)
			{
				if(i==0 || j==0)
				{
					if(	Math.abs(getx()+i*getCWidth()-m.getx())<m.getCWidth()/2 &&
						Math.abs(gety()+j*getCHeight()-m.gety())<m.getCHeight()/2)
					{
						if(tm.map[row+j][col+i]==0)
						return true;
					}
				}
			}
		}
		}
		return false;
	}
	
	public void update() {
		if(animation.getCounter()>=5)
			bum=true;
		if(!bum){
			if(currentAction != TICK) {
				currentAction = TICK;
				animation.setFrames(sprites.get(TICK));
				animation.setDelay(500);
			}
		}
		else if(bum) {
				if(currentAction != EXPLODE) {
					currentAction = EXPLODE;
					animation.setFrames(sprites.get(EXPLODE));
					animation.setDelay(100);
					//this.finalize();
				}
			}
		animation.update();
	}

}