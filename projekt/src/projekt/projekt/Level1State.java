package projekt;

import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

public class Level1State extends GameState {
	private Player player;
	private long start, elapsed;
	private Monster monster;
	private TileMap tileMap;
	private long score;
	public Level1State(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		start = System.currentTimeMillis();
		tileMap = new TileMap(32);
		tileMap.loadTiles("tileset1.gif");
		tileMap.loadMap(2);
		//tileMap.loadMap(2);
		tileMap.setPosition(0, 0);
		player = new Player(tileMap, 1);
		player.setPosition(48, 48);
		monster = new Monster(tileMap);
		monster.setPosition(13*32+16, 13*32+16);
		
	}
	
	public void update() {
		
		if( Math.abs(player.getx()-monster.getx())<monster.getCWidth()/2 &&
			Math.abs(player.gety()-monster.gety())<monster.getCHeight()/2)
		{
			monster.monstersound.playSound();
			JOptionPane.showMessageDialog(null, "GAME OVER");
			gsm.setState(0);
		}
		for(Bomb b : player.bombs)
		{
			if(b.hasCollision(tileMap, monster)){
				if(b.bum){
				
				//gsm.setState(0);
				gsm.setState(GameStateManager.LEVEL2STATE);
				}
				
				
			}
			if(b.hasCollision(tileMap, player)){
				if(b.bum){
					JOptionPane.showMessageDialog(null, "GAME OVER");
					gsm.setState(0);
					}
				}
		}
		player.update();
		monster.update();
		elapsed = System.currentTimeMillis() - start;
		elapsed /=100;
		score = 10000 - elapsed;
		if(elapsed>10000) score = 0;
		//elapsed = (System.nanoTime() - start)/1000;
		System.out.println(score);
	}
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		tileMap.draw(g);
		player.draw(g);
		player.drawBombs(g);
		monster.draw(g);
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(true);
		if(k == KeyEvent.VK_RIGHT) player.setRight(true);
		if(k == KeyEvent.VK_UP) player.setUp(true);
		if(k == KeyEvent.VK_DOWN) player.setDown(true);
		if(k == KeyEvent.VK_E) player.plantBomb();
		if(k == KeyEvent.VK_R) monster.setSpeed(0.9);
		//if(k == KeyEvent.VK_T) monster.setSpeed(0.01f);
		if(k == KeyEvent.VK_ESCAPE) {
			for(Bomb b : player.bombs)
			{
				b.bombExp.closeSound();
			}
			monster.monstersound.closeSound();
			System.exit(0);
		}
	}
	
	public void keyReleased(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(false);
		if(k == KeyEvent.VK_RIGHT) player.setRight(false);
		if(k == KeyEvent.VK_UP) player.setUp(false);
		if(k == KeyEvent.VK_DOWN) player.setDown(false);
	}
}