package projekt;



import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

public class Level2State extends GameState {
	private Player player;
	private Monster2 monster;
	private long start, elapsed;
	private TileMap tileMap;
	public Level2State(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		start = System.nanoTime();
		tileMap = new TileMap(32);
		tileMap.loadTiles("tileset1.gif");
		tileMap.loadMap(2);
		//tileMap.loadMap(2);
		tileMap.setPosition(0, 0);
		player = new Player(tileMap, 1);
		player.setPosition(48, 48);
		monster = new Monster2(tileMap);
		monster.setPosition(13*32+16, 13*32+16);
	}
	
	public void update() {
		
		if( Math.abs(player.getx()-monster.getx())<monster.getCWidth()/2 &&
			Math.abs(player.gety()-monster.gety())<monster.getCHeight()/2)
		{
			monster.monstersound.playSound();
			JOptionPane.showMessageDialog(null, "GAME OVER");
			gsm.setState(0);
		}
		for(Bomb b : player.bombs)
		{
			if(b.hasCollision(tileMap, monster)){
				if(b.bum){
					elapsed = System.nanoTime() - start;
					double score = 1/(elapsed*10000);
				JOptionPane.showMessageDialog(null, "YOU WON! YOUR SCORE IS " + score);
				gsm.setState(0);
				}
				//gsm.setState(1);
				
			}
			if(b.hasCollision(tileMap, player)){
				if(b.bum){
					JOptionPane.showMessageDialog(null, "GAME OVER");
					gsm.setState(0);
					}
				}
		}
		player.update();
		monster.update();
	}
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		tileMap.draw(g);
		player.draw(g);
		player.drawBombs(g);
		monster.draw(g);
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(true);
		if(k == KeyEvent.VK_RIGHT) player.setRight(true);
		if(k == KeyEvent.VK_UP) player.setUp(true);
		if(k == KeyEvent.VK_DOWN) player.setDown(true);
		if(k == KeyEvent.VK_E) player.plantBomb();
		if(k == KeyEvent.VK_ESCAPE) {
			for(Bomb b : player.bombs)
			{
				b.bombExp.closeSound();
			}
			monster.monstersound.closeSound();
			System.exit(0);
		}
	}
	
	public void keyReleased(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(false);
		if(k == KeyEvent.VK_RIGHT) player.setRight(false);
		if(k == KeyEvent.VK_UP) player.setUp(false);
		if(k == KeyEvent.VK_DOWN) player.setDown(false);
	}
}








