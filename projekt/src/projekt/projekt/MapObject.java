package projekt;

import java.util.Random;

public abstract class MapObject {
	
	protected TileMap tileMap;
	protected int tileSize;
	protected double x;
	protected double y;
	protected double dx;
	protected double dy;
	protected int width;
	protected int height;
	protected int cwidth;
	protected int cheight;
	protected double xdest;
	protected double ydest;
	protected double xtemp;
	protected double ytemp;
	protected boolean topLeft;
	protected boolean topRight;
	protected boolean bottomLeft;
	protected boolean bottomRight;
	protected Animation animation;
	protected int currentAction;
	protected boolean left;
	protected boolean right;
	protected boolean up;
	protected boolean down;
	protected double moveSpeed;
	protected boolean bot;
	
	public MapObject(TileMap tm) {
		tileMap = tm;
		tileSize = tm.getTileSize();
		bot = false;
	}
	
	public void calculateCorners(double x, double y) {
		
		int leftTile = (int)(x - cwidth / 2) / tileSize;
		int rightTile = (int)(x + cwidth / 2 - 1) / tileSize;
		int topTile = (int)(y - cheight / 2) / tileSize;
		int bottomTile = (int)(y + cheight / 2 - 1) / tileSize;
		
		int tl = tileMap.getType(topTile, leftTile);
		int tr = tileMap.getType(topTile, rightTile);
		int bl = tileMap.getType(bottomTile, leftTile);
		int br = tileMap.getType(bottomTile, rightTile);
		
		topLeft = tl == Tile.BLOCKED;
		topRight = tr == Tile.BLOCKED;
		bottomLeft = bl == Tile.BLOCKED;
		bottomRight = br == Tile.BLOCKED;
		
		
	}
	
public void calculateCorners2(double x, double y) {
		
		int leftTile = (int)(x - cwidth / 2) / tileSize;
		int rightTile = (int)(x + cwidth / 2 - 1) / tileSize;
		int topTile = (int)(y - cheight / 2) / tileSize;
		int bottomTile = (int)(y + cheight / 2 - 1) / tileSize;
		/*
		int tl = tileMap.getType(topTile, leftTile);
		int tr = tileMap.getType(topTile, rightTile);
		int bl = tileMap.getType(bottomTile, leftTile);
		int br = tileMap.getType(bottomTile, rightTile);
		*/
		int tl = tileMap.map[topTile][leftTile];
		int tr = tileMap.map[topTile][rightTile];
		int bl = tileMap.map[bottomTile][leftTile];
		int br = tileMap.map[bottomTile][rightTile];
		
		topLeft = tl == 3;
		topRight = tr == 3;
		bottomLeft = bl == 3;
		bottomRight = br == 3;
		
		
	}

	public void randomMove()
	{
		if(bot){
			int i=-1;
			Random generator = new Random();
			i = generator.nextInt(4);
			if(i==0){
				left = true;
				right = false;
				up = false;
				down = false;
			}
			else if(i==1){
				left = false;
				right = true;
				up = false;
				down = false;
			}
			else if(i==2){
				left = false;
				right = false;
				up = true;
				down = false;
			}
			else if(i==3){
				left = false;
				right = false;
				up = false;
				down = true;
			}
		}
	}
	
	
	
	public void checkTileMapCollision() {
	
		xdest = x + dx;
		ydest = y + dy;
		xtemp = x;
		ytemp = y;
		
		calculateCorners(x, ydest);
		
		if(bot){
		if(dy < 0) {
			if(topLeft || topRight) {
				dy = 0;
				if(up) randomMove();
			}
			else 
				ytemp += dy;
		}
		if(dy > 0) {
			if(bottomLeft || bottomRight) {
				dy = 0;
				if(down) randomMove();
			}
			else 
				ytemp += dy;
		}
		
		calculateCorners(xdest, y);
		if(dx < 0) {
			if(topLeft || bottomLeft) {
				dx = 0;
				if(left) randomMove();
			}
			else 
				xtemp += dx;
		}
		if(dx > 0) {
			if(topRight || bottomRight) {
				dx = 0;
				if(right) randomMove();
			}
			else 
				xtemp += dx;
		}
		
		
		
		}
		else{
			if(dy < 0) {
				if(topLeft || topRight) {
					dy = 0;
				}
				else 
					ytemp += dy;
			}
			if(dy > 0) {
				if(bottomLeft || bottomRight) {
					dy = 0;
				}
				else 
					ytemp += dy;
			}
			
			calculateCorners(xdest, y);
			if(dx < 0) {
				if(topLeft || bottomLeft) {
					dx = 0;
				}
				else 
					xtemp += dx;
			}
			if(dx > 0) {
				if(topRight || bottomRight) {
					dx = 0;
				}
				else 
					xtemp += dx;
			}
		}
	}
	
	public void checkTileMapCollision2() {
		
		xdest = x + dx;
		ydest = y + dy;
		xtemp = x;
		ytemp = y;
		int col = this.getx()/cwidth;
		int row = this.gety()/cheight;
		calculateCorners2(x, ydest);
		System.out.println((int)x%32 + ", " + (int)y%32);
		if(bot){
		if(dy < 0) {
			if(topLeft || topRight) {
				dy = 0;
				if(x%cwidth==17 && y%cheight==17 && col%2==1 && row%2==1)
				if(up) randomMove();
			}
			else 
				ytemp += dy;
			if(x%cwidth==17 && y%cheight==17 && col%2==1 && row%2==1)
				if(up) randomMove();
		}
		if(dy > 0) {
			if(bottomLeft || bottomRight) {
				dy = 0;
				if(x%cwidth==17 && y%cheight==17 && col%2==1 && row%2==1)
				if(down) randomMove();
			}
			else 
				ytemp += dy;
			if(x%cwidth==17 && y%cheight==17 && col%2==1 && row%2==1)
				if(down) randomMove();
		}
		
		calculateCorners2(xdest, y);
		if(dx < 0) {
			if(topLeft  || bottomLeft) {
				dx = 0;
				if(x%cwidth==16 && y%cheight==16 && col%2==1 && row%2==1){
				if(left) 
					randomMove();
				
				}
			}
			else 
				xtemp += dx;
			if(x%cwidth==16 && y%cheight==16 && col%2==1 && row%2==1){
				if(left) 
				randomMove();
			}
		}
		if(dx > 0) {
			if(topRight || bottomRight) {
				dx = 0;
				if(x%cwidth==16 && y%cheight==16 && col%2==1 && row%2==1)
				if(right) randomMove();
			}
			else 
				xtemp += dx;
			if(x%cwidth==16 && y%cheight==16 && col%2==1 && row%2==1)
				if(right) randomMove();
		}
		
		}
		else{
			if(dy < 0) {
				if(topLeft || topRight) {
					dy = 0;
				}
				else 
					ytemp += dy;
			}
			if(dy > 0) {
				if(bottomLeft || bottomRight) {
					dy = 0;
				}
				else 
					ytemp += dy;
			}
			
			calculateCorners2(xdest, y);
			if(dx < 0) {
				if(topLeft || bottomLeft) {
					dx = 0;
				}
				else 
					xtemp += dx;
			}
			if(dx > 0) {
				if(topRight || bottomRight) {
					dx = 0;
				}
				else 
					xtemp += dx;
			}
		}
	}
	
	public int getx() { return (int)x; }
	public int gety() { return (int)y; }
	public int getWidth() { return width; }
	public int getHeight() { return height; }
	public int getCWidth() { return cwidth; }
	public int getCHeight() { return cheight; }
	
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSpeed(double s) { moveSpeed = s; }
	public void setLeft(boolean b) { left = b; }
	public void setRight(boolean b) { right = b; }
	public void setUp(boolean b) { up = b; }
	public void setDown(boolean b) { down = b; }
		
}