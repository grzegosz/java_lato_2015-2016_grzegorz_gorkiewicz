package projekt;

import java.awt.Graphics2D;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class MenuState extends GameState {
	
	private Sound pop0 = new Sound("D:/Programy/Eclipse wokrspace/projekt/src/projekt/projekt/pop0.wav");
	private Sound pop1 = new Sound("D:/Programy/Eclipse wokrspace/projekt/src/projekt/projekt/pop1.wav");
	private BufferedImage bg;
	
	private int currentChoice = 0;
	private String[] options = {
		"Play",
		"1 vs 1",
		"Help",
		"Quit"
	};
	
	private Color titleColor;
	private Font titleFont;
	
	private Font font;
	
	public MenuState(GameStateManager gsm) {
		
		this.gsm = gsm;
		
		try {
			bg = ImageIO.read(getClass().getResourceAsStream("bomb_bg.jpg"));
			
			titleColor = Color.WHITE;
			titleFont = new Font(
					"Calibri",
					Font.BOLD,
					36);
			
			font = new Font("Arial", Font.PLAIN, 18);
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void init() {}
	
	public void update() {}
	
	public void draw(Graphics2D g) {
		
		// draw bg
		g.drawImage(bg, 0, 0, null);
		
		// draw title
		g.setColor(Color.gray);
		g.fillRect(10, 220, 150, 200);
		g.setColor(titleColor);
		g.setFont(titleFont);
		g.drawString("BOMBERMAN", 240, 100);
		g.setFont(new Font("Arial", Font.PLAIN, 12));
		g.drawString("~ G. G.", 580, 440);
		// draw menu options
		g.setFont(font);
		for(int i = 0; i < options.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.WHITE);
			}
			else {
				g.setColor(Color.BLACK);
			}
			g.drawString(options[i], 50, 250 + i * 50);
		}
		
	}
	
	private void select() {
		if(currentChoice == 0) {
			//play
			gsm.setState(GameStateManager.LEVEL1STATE);
		}
		if(currentChoice == 1) {
			// online
			gsm.setState(GameStateManager.VSSTATE);
		}
		if(currentChoice == 2) {
			// help
		}
		if(currentChoice == 3) {
			
			System.exit(0);
		}
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			pop0.playSound();
			select();
		}
		if(k == KeyEvent.VK_UP) {
			pop1.playSound();
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			pop1.playSound();
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
		if(k == KeyEvent.VK_ESCAPE){
			pop0.closeSound();
			pop1.closeSound();
			System.exit(0);
		}
	}
	public void keyReleased(int k) {}
}