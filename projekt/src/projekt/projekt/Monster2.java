package projekt;

import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Monster2 extends MapObject{
	
	private boolean isAlive;
	
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {4};
	public Sound monstersound = new Sound("src/projekt/projekt/monster.wav");;
	private static final int ANIM  = 0;
	
	public Monster2(TileMap tm) {
		super(tm);
		isAlive = true;
		bot = true;
		left = true;
		right = false;
		up = false;
		down = false;
		width = 32;
		height = 32;
		cwidth = 32;
		cheight = 32;
		moveSpeed = 0.01;
		try {
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("monster3.png"));
			sprites = new ArrayList<BufferedImage[]>();
			for(int i = 0; i < 1; i++) {
				BufferedImage[] bi = new BufferedImage[numFrames[i]];
				if(i!=0)
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width +  (i-1)*numFrames[i] * width,0,width,height);
					}
				}
				else
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width,0,width,height);
					}
				}
				sprites.add(bi);	
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		animation = new Animation();
		currentAction = ANIM;
		animation.setFrames(sprites.get(ANIM));
		animation.setDelay(400);
		
	}
	
	private void getNextPosition() {
		
		if(left) 		dx = -moveSpeed;
		else if(right)	dx = moveSpeed;
		else			dx = 0;
		
		if(up) 			dy = -moveSpeed;
		else if(down) 	dy = moveSpeed;
		else 			dy = 0;
		
	}
	
	public void update() {
		
		getNextPosition();
		checkTileMapCollision2();
		setPosition(xtemp, ytemp);
		animation.update();
	}
	
	
	public void draw(Graphics2D g) {
			g.drawImage(
				animation.getImage(),
				(int)(x  - width / 2),
				(int)(y  - height / 2),
				null
			);		
	}
}


