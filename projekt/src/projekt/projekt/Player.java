package projekt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends MapObject{
	
	private boolean isAlive;
	public ArrayList<Bomb> bombs;
	private int maxBombs;
	
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {
		1, 3, 3, 3, 3
	};
	
	private static final int IDLE  = 0;
	private static final int DOWN  = 1;
	private static final int RIGHT = 2;
	private static final int LEFT  = 3;
	private static final int UP    = 4;
	
	public Player(TileMap tm, int n) {
		super(tm);
		maxBombs = 2;
		bombs = new ArrayList<Bomb>();
		isAlive = true;
		width = 24;
		height = 32;
		cwidth = 28;
		cheight = 26;
		moveSpeed = 2;
		try {
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("bomberman11.png"));
			if(n==2){
				spritesheet = ImageIO.read(getClass().getResourceAsStream("bomberman33.png"));
			}
			sprites = new ArrayList<BufferedImage[]>();
			for(int i = 0; i < 5; i++) {
				BufferedImage[] bi = new BufferedImage[numFrames[i]];
				if(i!=0)
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width +  (i-1)*numFrames[i] * width,0,width,height);
					}
				}
				else
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width,0,width,height);
					}
				}
				sprites.add(bi);	
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		animation = new Animation();
		currentAction = IDLE;
		animation.setFrames(sprites.get(IDLE));
		animation.setDelay(200);
		
	}
	
	private void getNextPosition() {
		
		if(left) 		dx = -moveSpeed;
		else if(right)	dx = moveSpeed;
		else			dx = 0;
		
		if(up) 			dy = -moveSpeed;
		else if(down) 	dy = moveSpeed;
		else 			dy = 0;
		
	}
	
	public void update() {
		
		getNextPosition();
		checkTileMapCollision();
		setPosition(xtemp, ytemp);
		
		if(left) {
			if(currentAction != LEFT) {
				currentAction = LEFT;
				animation.setFrames(sprites.get(LEFT));
				animation.setDelay(200);
			}
		}
		else if(right) {
			if(currentAction != RIGHT) {
				currentAction = RIGHT;
				animation.setFrames(sprites.get(RIGHT));
				animation.setDelay(200);
			}
		}
		else if(up) {
			if(currentAction != UP) {
				currentAction = UP;
				animation.setFrames(sprites.get(UP));
				animation.setDelay(200);
			}
		}
		else if(down) {
			if(currentAction != DOWN) {
				currentAction = DOWN;
				animation.setFrames(sprites.get(DOWN));
				animation.setDelay(200);
			}
		}
		else {
			if(currentAction != IDLE) {
				currentAction = IDLE;
				animation.setFrames(sprites.get(IDLE));
				animation.setDelay(100);
			}
		}
		animation.update();
		for(Bomb b : bombs)
			b.update();
	}
	
	public void plantBomb()
	{
		if(bombs.size()<maxBombs)
		{
		Bomb bomb = new Bomb(tileMap);
		bomb.x = (int)(x/32) * 32 + 16;
		bomb.y = (int)(y/32) * 32 + 16;
		bombs.add(bomb);
		}
	}
	
	public void draw(Graphics2D g) {
			g.drawImage(
				animation.getImage(),
				(int)(x  - width / 2),
				(int)(y  - height / 2),
				null
			);		
	}
	
	public void drawBombs(Graphics2D g) {
	Iterator<Bomb> it = bombs.iterator();
	 Set<Bomb> tmp= new HashSet<Bomb>(); 
	 int bx, by;
     while (it.hasNext()) { 
             Bomb b = (Bomb) it.next();
             bx = (int)(b.x  - b.width / 2);
             by = (int)(b.y  - b.height / 2);
             if(!b.bum)
 				g.drawImage((b.animation).getImage(),bx,by,null);
 			else{
 				b.explode(tileMap, g);
 				
 				//
 			}
             if(b.bum && b.animation.hasPlayedOnce()) 
             {
            	 tmp.add(b);
            	// b.Clear(tileMap);
             }
     } 
    bombs.removeAll(tmp); 
		/*
	for(Bomb b : bombs)
	{
			if(!b.bum)
				g.drawImage((b.animation).getImage(),(int)(b.x  - b.width / 2),(int)(b.y  - b.height / 2),null);
			else{
				b.explode(tileMap, g);
				
			}
	}		
	*/
	}
}


