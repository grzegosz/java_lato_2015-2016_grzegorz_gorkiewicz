package projekt;

import java.io.File;

import javax.sound.sampled.*;

public class Sound {

	private String filename;
	private Clip clip;
	
	public void SetFilename(String name){
		filename = name;
	}
	public String GetFilename(){
		return filename;
		
	}
	
	public Sound(String name){
		filename = name;
		try {
	       // AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getAbsoluteFile());
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getCanonicalFile());
	        clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        //clip.start();
	    } catch(Exception e) {
	        System.out.println("Error");
	        e.printStackTrace();
	    }
	}
	
	public void playSound() {
	        clip.start();
	}
	
	public void closeSound() {
			clip.close();
	}
}
