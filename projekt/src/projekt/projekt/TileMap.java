package projekt;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.Random;

import javax.imageio.ImageIO;

//import Main.GamePanel;

public class TileMap {
	
	private double x;
	private double y;
	
	public int[][] map;
	private int tileSize;
	private int numRows;
	private int numCols;
	private int width;
	private int height;
	
	private BufferedImage tileset;
	private int numTilesAcross;
	public Tile[][] tiles;
	
	private int rowOffset;
	private int colOffset;
	private int numRowsToDraw;
	private int numColsToDraw;
	
	public TileMap(int tileSize) {
		this.tileSize = tileSize;
		numRowsToDraw = GamePanel.HEIGHT / tileSize + 2;
		numColsToDraw = GamePanel.WIDTH / tileSize + 2;
	}
	
	public void loadTiles(String s) {
		
		try {

			tileset = ImageIO.read(getClass().getResourceAsStream(s));
			numTilesAcross = tileset.getWidth() / tileSize;
			tiles = new Tile[2][numTilesAcross];
			
			BufferedImage subimage;
			for(int col = 0; col < numTilesAcross; col++) {
				subimage = tileset.getSubimage(col * tileSize,0,tileSize,tileSize);
				tiles[0][col] = new Tile(subimage, Tile.NORMAL);
				subimage = tileset.getSubimage(col * tileSize,tileSize,tileSize,tileSize);
				tiles[1][col] = new Tile(subimage, Tile.BLOCKED);
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void loadMap(int n) {
		
		try {
			
			//InputStream in = getClass().getResourceAsStream(s);
			//BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			numCols = 15;
			numRows = 15;
			map = new int[numRows][numCols];
			width = numCols * tileSize;
			height = numRows * tileSize;
			
			if(n==3){
				for(int row=0; row < numRows; row++)
				{
					for(int col=0; col < numCols; col++)
					{
						
						if(row==0 || col==0 || row==numRows-1 || col==numCols-1 ||(row%2==0 && col%2==0)) map[row][col]=3;
						else
						{
							map[row][col]=1;
						}
						
					}
				}
			}
			else{
			int grass=-3;
			for(int row=0; row < numRows; row++)
			{
				for(int col=0; col < numCols; col++)
				{
					
					if(row==0 || col==0 || row==numRows-1 || col==numCols-1 ||(row%2==0 && col%2==0)) map[row][col]=3;
					else
					{
						map[row][col]=1;
						grass++;
					}
					
				}
			}
			Random rand = new Random();
			boolean brick = false;
			int numBricks=0;
			while(numBricks<(2*grass)/3)
			numBricks = rand.nextInt(grass) - 3;
			int maxBricks0 = numCols/2 -1;
			int minBricks0 = numCols/4 -1;
			int maxBricks1 = numCols -2 -2;
			int minBricks1 = numCols/2 -2;
			int bricks = 0;
			int min = 0;
			int max = 0;
			for(int row=1; row<numRows-1; row++)
			{
				if(row%2==0){
							max = maxBricks0;
							min = minBricks0;
							}
				else 		{
							max = maxBricks1;
							min = minBricks1;
							}
				
				bricks = (numBricks + min)/max;
				
				for(int col=1; col<numCols-1; col++)
				{
					brick = rand.nextBoolean();
					if((!(row==1 && col==1) && !(row==2 && col==1) && !(row==1 && col==2))&& map[row][col]==1 && bricks > 0  && brick && numBricks>0 )
					{
						map[row][col]=2;
						numBricks--;
						//max--;
					}
				}
				if(n==2){map[13][13]=1;
						 map[13][12]=1;
						 map[12][13]=1;}
			}
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public int getTileSize() { return tileSize; }
	public int getx() { return (int)x; }
	public int gety() { return (int)y; }
	public int getWidth() { return width; }
	public int getHeight() { return height; }
	public void setType(int col, int row, int t)
	{
		tiles[row][col].setType(t);
	}
	public int getType(int row, int col) {
		int rc = map[row][col];
		int r = rc / numTilesAcross;
		int c = rc % numTilesAcross;
		return tiles[r][c].getType();
	}
	
	public void setPosition(double x, double y) {
		
		this.x += (x - this.x);
		this.y += (y - this.y);
		
		colOffset = (int)-this.x / tileSize;
		rowOffset = (int)-this.y / tileSize;
		
	}
	
	
	public void draw(Graphics2D g) {
		
		for(int row = rowOffset;row < rowOffset + numRowsToDraw;row++) {
			
			if(row >= numRows) break;
			
			for(int col = colOffset;col < colOffset + numColsToDraw;col++) {
				
				if(col >= numCols) break;
				
				
				int rc = map[row][col];
				int r = rc / numTilesAcross;
				int c = rc % numTilesAcross;
				
				g.drawImage(tiles[r][c].getImage(),(int)x + col * tileSize,(int)y + row * tileSize,null);
			}
			
		}
		
	}
	
}



















