package projekt;

import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;


public class VSState extends GameState {
private Player player;
private Player player2;	

	private TileMap tileMap;
	public VSState(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		
		tileMap = new TileMap(32);
		tileMap.loadTiles("tileset1.gif");
		tileMap.loadMap(2);
		tileMap.setPosition(0, 0);
		player = new Player(tileMap, 1);
		player.setPosition(48, 48);
		player2 = new Player(tileMap, 2);
		player2.setPosition(13*32+16, 13*32+16);
	}
	
	public void update() {
		for(Bomb b : player.bombs)
		{
			if(b.hasCollision(tileMap, player2)){
				if(b.bum){
				JOptionPane.showMessageDialog(null, "P1 WON");
				gsm.setState(0);}
			}
			if(b.hasCollision(tileMap, player)){
				if(b.bum){
					JOptionPane.showMessageDialog(null, "P2 WON");
					gsm.setState(0);}
				}
		}
		for(Bomb b : player2.bombs)
		{
			if(b.hasCollision(tileMap, player2)){
				if(b.bum){
				JOptionPane.showMessageDialog(null, "P1 WON");
				gsm.setState(0);}
			}
			if(b.hasCollision(tileMap, player)){
				if(b.bum){
					JOptionPane.showMessageDialog(null, "P2 WON");
					gsm.setState(0);}
				}
		}
		player.update();
		player2.update();
	}
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		tileMap.draw(g);
		player.draw(g);
		player.drawBombs(g);
		player2.draw(g);
		player2.drawBombs(g);
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(true);
		if(k == KeyEvent.VK_RIGHT) player.setRight(true);
		if(k == KeyEvent.VK_UP) player.setUp(true);
		if(k == KeyEvent.VK_DOWN) player.setDown(true);
		if(k == KeyEvent.VK_ENTER) player.plantBomb();
		
		if(k == KeyEvent.VK_A) player2.setLeft(true);
		if(k == KeyEvent.VK_D) player2.setRight(true);
		if(k == KeyEvent.VK_W) player2.setUp(true);
		if(k == KeyEvent.VK_S) player2.setDown(true);
		if(k == KeyEvent.VK_E) player2.plantBomb();
		
		if(k == KeyEvent.VK_ESCAPE) {
			for(Bomb b : player.bombs)
			{
				b.bombExp.closeSound();
			}
			for(Bomb b : player2.bombs)
			{
				b.bombExp.closeSound();
			}
			System.exit(0);
			}
	}
	
	public void keyReleased(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(false);
		if(k == KeyEvent.VK_RIGHT) player.setRight(false);
		if(k == KeyEvent.VK_UP) player.setUp(false);
		if(k == KeyEvent.VK_DOWN) player.setDown(false);
		
		if(k == KeyEvent.VK_A) player2.setLeft(false);
		if(k == KeyEvent.VK_D) player2.setRight(false);
		if(k == KeyEvent.VK_W) player2.setUp(false);
		if(k == KeyEvent.VK_S) player2.setDown(false);
	}
}










