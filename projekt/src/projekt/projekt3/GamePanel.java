package projekt;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.*;
public class GamePanel extends JPanel implements Runnable, KeyListener{

	public static int WIDTH = 480;
	public static int HEIGHT = 480;
	private Thread watek;
	boolean running;
	private BufferedImage image;
	private Graphics2D g;
	private GameStateManager gsm;
	
	public GamePanel(){
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
	}
	
	public void addNotify(){
		super.addNotify();
		if(watek == null){
			watek = new Thread(this);
			addKeyListener(this);
			watek.start();
		}
	}
	
	public void run(){
		running = true;
		gsm = new GameStateManager();
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		
		while(running){		//game loop
			gameUpdate();
			gameRender();
			gameDraw();	
			}
			
	}
	
	private void gameUpdate() {
		gsm.update();
	}
	
	private void gameRender(){
		gsm.draw(g);
	}
	
	private void gameDraw(){
		Graphics g2 = this.getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}
	
	public void keyTyped(KeyEvent key) {}
	public void keyPressed(KeyEvent key) {
		gsm.keyPressed(key.getKeyCode());
	}
	public void keyReleased(KeyEvent key) {
		gsm.keyReleased(key.getKeyCode());
	}
}
