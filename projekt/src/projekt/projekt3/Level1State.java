package projekt;



import java.awt.*;
import java.awt.event.KeyEvent;

public class Level1State extends GameState {
	
	private TileMap tileMap;
	public Level1State(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		
		tileMap = new TileMap(32);
		tileMap.loadTiles("tileset1.gif");
		tileMap.loadMap("Lvl01.map");
		tileMap.setPosition(0, 0);
	}
	
	public void update() {
	}
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		tileMap.draw(g);
	}
	
	public void keyPressed(int k) {
	}
	
	public void keyReleased(int k) {
	}
}












