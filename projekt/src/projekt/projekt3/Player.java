package projekt;

import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends MapObject {
	
	// player stuff
	private int health;
	private int maxHealth;
	//private boolean dead;
	private boolean flinching;
	private long flinchTimer;
	
	// animations
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {
		1, 3, 3, 3, 3
	};
	
	// animation actions
	private static final int IDLE  = 0;
	private static final int DOWN  = 1;
	private static final int RIGHT = 2;
	private static final int LEFT  = 3;
	private static final int UP    = 4;
	
	public Player(TileMap tm) {
		
		super(tm);
		
		width = 24;
		height = 32;
		cwidth = 20;
		cheight = 20;
		
		moveSpeed = 0.3;
		maxSpeed = 1.6;
		stopSpeed = 0.4;
		
		//facingRight = true;
		
		// load sprites
		try {
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("bomberman11.png"));
			
			sprites = new ArrayList<BufferedImage[]>();
			for(int i = 0; i < 5; i++) {
				BufferedImage[] bi = new BufferedImage[numFrames[i]];
				if(i!=0)
				{
				for(int j = 0; j < numFrames[i]; j++) {
					bi[j] = spritesheet.getSubimage(j * width,0,width,height);
					}
				}
				else
				{
					for(int j = 0; j < numFrames[i]; j++) {
						bi[j] = spritesheet.getSubimage(j * width,0,width,height);
					}
				}
				sprites.add(bi);	
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		animation = new Animation();
		currentAction = IDLE;
		animation.setFrames(sprites.get(IDLE));
		animation.setDelay(400);
		
	}
	
	public int getHealth() { return health; }
	public int getMaxHealth() { return maxHealth; }
	
	private void getNextPosition() {
		
		// movement
		if(left) {
			dx -= moveSpeed;
			if(dx < -maxSpeed) {
				dx = -maxSpeed;
			}
		}
		else if(right) {
			dx += moveSpeed;
			if(dx > maxSpeed) {
				dx = maxSpeed;
			}
		}
		else {
			if(dx > 0) {
				dx -= stopSpeed;
				if(dx < 0) {
					dx = 0;
				}
			}
			else if(dx < 0) {
				dx += stopSpeed;
				if(dx > 0) {
					dx = 0;
				}
			}
		}
	}
	
	public void update() {
		
		// update position
		getNextPosition();
		checkTileMapCollision();
		setPosition(xtemp, ytemp);
		
		// set animation
		
		if(left) {
			if(currentAction != LEFT) {
				currentAction = LEFT;
				animation.setFrames(sprites.get(LEFT));
				animation.setDelay(40);
				width = 24;
			}
		}
		else if(right) {
			if(currentAction != RIGHT) {
				currentAction = RIGHT;
				animation.setFrames(sprites.get(RIGHT));
				animation.setDelay(40);
				width = 24;
			}
		}
		else if(up) {
			if(currentAction != UP) {
				currentAction = UP;
				animation.setFrames(sprites.get(UP));
				animation.setDelay(40);
				width = 24;
			}
		}
		else if(down) {
			if(currentAction != DOWN) {
				currentAction = DOWN;
				animation.setFrames(sprites.get(DOWN));
				animation.setDelay(40);
				width = 24;
			}
		}
		else {
			if(currentAction != IDLE) {
				currentAction = IDLE;
				animation.setFrames(sprites.get(IDLE));
				animation.setDelay(400);
				width = 24;
			}
		}
		
		animation.update();
		
		// set direction
		
	}
	
	public void draw(Graphics2D g) {
		
		setMapPosition();
		
		// draw player
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed / 100 % 2 == 0) {
				return;
			}
		}
		/*
		if(facingRight) {
			g.drawImage(
				animation.getImage(),
				(int)(x + xmap - width / 2),
				(int)(y + ymap - height / 2),
				null
			);
		}
		else {
			g.drawImage(
				animation.getImage(),
				(int)(x + xmap - width / 2 + width),
				(int)(y + ymap - height / 2),
				-width,
				height,
				null
			);	
		}*/	
	}
	
	
	
}
